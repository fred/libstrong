find_package(Boost 1.53.0
    COMPONENTS
        program_options
        unit_test_framework
    REQUIRED)

add_executable(test-strong
    main.cc
    test_skills.cc)

add_executable(LibStrong::test ALIAS test-strong)

#Set target properties
target_include_directories(test-strong
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR})

target_compile_features(test-strong PRIVATE cxx_decltype_auto)
target_compile_options(test-strong PRIVATE $<$<CXX_COMPILER_ID:GNU>:-Wall -Wextra -ggdb -grecord-gcc-switches>)

target_link_libraries(test-strong PRIVATE
    LibStrong::library
    Boost::program_options
    Boost::unit_test_framework)
