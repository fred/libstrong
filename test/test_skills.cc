/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libstrong/type.hh"

#include <boost/test/unit_test.hpp>

#include <sstream>
#include <string>
#include <utility>

BOOST_AUTO_TEST_SUITE(TestSkills)

namespace {

using Id = LibStrong::Type<int, struct IdTag_, LibStrong::Collect<LibStrong::Skill::Sequenceable, LibStrong::Skill::Streamable>::SuperSkill>;
using ComparableStreamable = LibStrong::Type<int, struct RandomIdTag_, LibStrong::Collect<LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>::SuperSkill>;
using StringId = LibStrong::Type<std::string, struct StringIdTag_, LibStrong::Collect<LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>::SuperSkill>;
using Size = LibStrong::Type<int, struct SizeTag_, LibStrong::Collect<LibStrong::Skill::Countable, LibStrong::Skill::Streamable>::SuperSkill>;

using FirstName = LibStrong::Type<std::string, struct FirstNameTag_, LibStrong::Collect<LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>::SuperSkill>;
using MiddleName = LibStrong::Type<std::string, struct MiddleNameTag_, LibStrong::Collect<LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>::SuperSkill>;
using LastName = LibStrong::Type<std::string, struct LastNameTag_, LibStrong::Collect<LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>::SuperSkill>;
using FullName = LibStrong::Type<std::string, struct FullNameTag_, LibStrong::Collect<LibStrong::Skill::Comparable, LibStrong::Skill::Streamable>::SuperSkill>;

FullName compose(const FirstName& first_name, const MiddleName::Optional& middle_name, const LastName& last_name)
{
    BOOST_CHECK_EQUAL(middle_name == MiddleName::nullopt, !(middle_name != MiddleName::nullopt));
    return FullName{middle_name == MiddleName::nullopt ? *first_name + " " + *last_name
                                                       : *first_name + " " + **middle_name + " " + *last_name};
}

template <typename Type>
void check_output(Type&& value, const char* str)
{
    std::ostringstream out;
    out << std::forward<Type>(value);
    BOOST_CHECK_EQUAL(out.str(), str);
}

template <typename LhsType, typename RhsType>
void check_increment()
{
    auto value = LhsType{1};
    BOOST_CHECK_EQUAL(value, LhsType{1});
    BOOST_CHECK_EQUAL(++value, LhsType{2});
    BOOST_CHECK_EQUAL(value++, LhsType{2});
    BOOST_CHECK_EQUAL(value, LhsType{3});
    BOOST_CHECK_EQUAL(value += RhsType{2}, LhsType{5});
}

template <typename LhsType, typename RhsType>
void check_decrement()
{
    auto value = LhsType{3};
    BOOST_CHECK_EQUAL(value, LhsType{3});
    BOOST_CHECK_EQUAL(--value, LhsType{2});
    BOOST_CHECK_EQUAL(value--, LhsType{2});
    BOOST_CHECK_EQUAL(value, LhsType{1});
    BOOST_CHECK_EQUAL(value -= RhsType{2}, LhsType{-1});
}

template <typename LhsType, typename RhsType>
void check_comparison_consistency(const LhsType& lhs, const RhsType& rhs)
{
    if (lhs < rhs)
    {
        BOOST_CHECK_LE(lhs, rhs);
        BOOST_CHECK(!(lhs == rhs));
        BOOST_CHECK_NE(lhs, rhs);
        BOOST_CHECK(!(lhs >= rhs));
        BOOST_CHECK(!(lhs > rhs));
    }
    else if (lhs == rhs)
    {
        BOOST_CHECK_LE(lhs, rhs);
        BOOST_CHECK(!(lhs != rhs));
        BOOST_CHECK_GE(lhs, rhs);
        BOOST_CHECK(!(lhs > rhs));
    }
    else
    {
        BOOST_CHECK(!(lhs <= rhs));
        BOOST_CHECK_NE(lhs, rhs);
        BOOST_CHECK_GE(lhs, rhs);
        BOOST_CHECK_GT(lhs, rhs);
    }
}

template <typename LhsType, typename RhsType>
void check_lesser(const LhsType& lhs, const RhsType& rhs)
{
    BOOST_CHECK_LT(lhs, rhs);
    BOOST_CHECK_GT(rhs, lhs);
    check_comparison_consistency(lhs, rhs);
    check_comparison_consistency(rhs, lhs);
}

template <typename LhsType, typename RhsType>
void check_equal(const LhsType& lhs, const RhsType& rhs)
{
    BOOST_CHECK(lhs == rhs);
    BOOST_CHECK(rhs == lhs);
    check_comparison_consistency(lhs, rhs);
    check_comparison_consistency(rhs, lhs);
}

}//namespace {anonymous}

BOOST_AUTO_TEST_CASE(test_output)
{
    check_output(Id{1}, "1");
    check_output(ComparableStreamable{1}, "1");
    check_output(StringId{"1"}, "1");
    check_output(Size{1}, "1");
}

BOOST_AUTO_TEST_CASE(test_increment)
{
    check_increment<Id, int>();
    check_increment<Size, Size>();
}

BOOST_AUTO_TEST_CASE(test_decrement)
{
    check_decrement<Id, int>();
    check_decrement<Size, Size>();
}

BOOST_AUTO_TEST_CASE(test_string_id)
{
    BOOST_CHECK_EQUAL(compose(FirstName{"Karel"}, MiddleName{"von"}, LastName{"Bahnhof"}), FullName{"Karel von Bahnhof"});
    BOOST_CHECK_EQUAL(compose(FirstName{"Karel"}, MiddleName::nullopt, LastName{"Bahnhof"}), FullName{"Karel Bahnhof"});
}

BOOST_AUTO_TEST_CASE(test_optional_id)
{
    using Optional = LibStrong::Optional<Id>;
    BOOST_CHECK(Optional{} == Id::nullopt);
    BOOST_CHECK(is_nullopt(Optional{}));
    BOOST_CHECK(!is_nullopt(Optional{Id{1}}));
    BOOST_CHECK(!has_value(Optional{}));
    BOOST_CHECK(has_value(Optional{Id{1}}));
    BOOST_CHECK_EQUAL((Optional{false, []() { return Id{}; }}), Optional{});
    BOOST_CHECK_EQUAL((Optional{true, []() { return Id{0}; }}), Id{0});
    BOOST_CHECK_EQUAL((Optional{true, []() { return Id{1}; }}), Id{1});
    BOOST_CHECK_EQUAL((Optional{true, []() { return Id{7}; }}), Id{7});
#ifdef LIBSTRONG_HAS_IN_PLACE
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place}), Id{});
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place, 1}), Id{1});

    check_lesser(Optional{LibStrong::in_place, 1}, Optional{LibStrong::in_place, 2});
    check_equal(Optional{LibStrong::in_place, 2}, Optional{LibStrong::in_place, 2});

    check_lesser(Optional{LibStrong::in_place, 1}, Id{2});
    check_equal(Optional{LibStrong::in_place, 2}, Id{2});

    check_lesser(Id{1}, Optional{LibStrong::in_place, 2});
    check_equal(Id{2}, Optional{LibStrong::in_place, 2});

    check_lesser(Optional{}, Optional{LibStrong::in_place, 2});
    check_equal(Optional{}, Optional{});
#else
    BOOST_CHECK_EQUAL((Optional{Id{}}), Id{});
    BOOST_CHECK_EQUAL((Optional{Id{1}}), Id{1});

    check_lesser(Optional{Id{1}}, Optional{Id{2}});
    check_equal(Optional{Id{2}}, Optional{Id{2}});

    check_lesser(Optional{Id{1}}, Id{2});
    check_equal(Optional{Id{2}}, Id{2});

    check_lesser(Id{1}, Optional{Id{2}});
    check_equal(Id{2}, Optional{Id{2}});

    check_lesser(Optional{}, Optional{Id{2}});
    check_equal(Optional{}, Optional{});
#endif
}

BOOST_AUTO_TEST_CASE(test_optional_string_id)
{
    using Optional = LibStrong::Optional<StringId>;
    BOOST_CHECK(Optional{} == StringId::nullopt);
    BOOST_CHECK((Optional{false, []() { return StringId{}; }}) == StringId::nullopt);
    BOOST_CHECK_EQUAL((Optional{true, []() { return StringId{}; }}), StringId{});
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place, ""}), StringId{});
    BOOST_CHECK_EQUAL(**(Optional{LibStrong::in_place, "foo"}), "foo");
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place, "foo", 0u}), StringId{""});
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place, "foo", 1u}), StringId{"f"});
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place, "foo", 3u}), StringId{"foo"});
    BOOST_CHECK_EQUAL((Optional{LibStrong::in_place, "foo", 1u, 2u}), StringId{"oo"});

#ifdef LIBSTRONG_HAS_IN_PLACE
    check_lesser(Optional{LibStrong::in_place, "a"}, Optional{LibStrong::in_place, "b"});
    check_equal(Optional{LibStrong::in_place, "b"}, Optional{LibStrong::in_place, "b"});

    check_lesser(Optional{LibStrong::in_place, "a"}, StringId{"b"});
    check_equal(Optional{LibStrong::in_place, "b"}, StringId{"b"});

    check_lesser(StringId{"a"}, Optional{LibStrong::in_place, "b"});
    check_equal(StringId{"b"}, Optional{LibStrong::in_place, "b"});

    check_lesser(Optional{}, Optional{LibStrong::in_place, "b"});
    check_equal(Optional{}, Optional{});
#else
    check_lesser(Optional{StringId{"a"}}, Optional{StringId{"b"}});
    check_equal(Optional{StringId{"b"}}, Optional{StringId{"b"}});

    check_lesser(Optional{StringId{"a"}}, StringId{"b"});
    check_equal(Optional{StringId{"b"}}, StringId{"b"});

    check_lesser(StringId{"a"}, Optional{StringId{"b"}});
    check_equal(StringId{"b"}, Optional{StringId{"b"}});

    check_lesser(Optional{}, Optional{StringId{"b"}});
    check_equal(Optional{}, Optional{});
#endif
}

BOOST_AUTO_TEST_SUITE_END()//TestSkills
