CHANGELOG
=========

.. contents:: Releases
   :backlinks: none
   :local:


1.1.1 (2022-02-07)
------------------

* CMake build updated

1.1.0 (2021-09-09)
------------------

* CMake build reworked - dependencies and bumpversion

1.0.0 (2021-03-24)
------------------

* Initial release
