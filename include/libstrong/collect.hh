/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COLLECT_HH_DB340D308829C098EDBC78EA1D3BBE09//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COLLECT_HH_DB340D308829C098EDBC78EA1D3BBE09

#include <type_traits>

namespace LibStrong {

template <template <typename> class ...Skills>
struct Collect
{
    template <typename Derived>
    struct SuperSkill : Skills<Derived>...
    { };
};

}//namespace LibStrong

#endif//COLLECT_HH_DB340D308829C098EDBC78EA1D3BBE09
