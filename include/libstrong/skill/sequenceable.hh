/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SEQUENCEABLE_HH_947C330D84CD8E902561DCB5BB5CBD74//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define SEQUENCEABLE_HH_947C330D84CD8E902561DCB5BB5CBD74

#include "libstrong/skill/comparable.hh"

#include <type_traits>
#include <utility>

namespace LibStrong {
namespace Skill {

// Skill Sequenceable offers operations adapted to arithmetic sequences, it means:
//
//   1) addition with underlying type: Sequenceable<T> + T -> Sequenceable<T>
//      * shift in a sequence by N positions
//   2) subtraction with underlying type: Sequenceable<T> - T -> Sequenceable<T>
//      * shift in a sequence by N positions (in opposite direction)
//   3) difference: Sequenceable<T> - Sequenceable<T> -> T
//      * difference between two positions
//   4) (pre/post)increment:
//      * increment position in a sequence
//   5) (pre/post)decrement:
//      * decrement position in a sequence
//   6) comparison operations: Sequenceable<T> cmp Sequenceable<T> -> bool
//      * compares both values
//
// enabled operations:
//     Sequenceable<T>  +  T               -> Sequenceable<T>
//     Sequenceable<T>  -  T               -> Sequenceable<T>
//     Sequenceable<T>  -  Sequenceable<T> -> T
//   ++Sequenceable<T>                     -> Sequenceable<T>
//     Sequenceable<T>++                   -> Sequenceable<T>
//   --Sequenceable<T>                     -> Sequenceable<T>
//     Sequenceable<T>--                   -> Sequenceable<T>
//     Sequenceable<T> cmp Sequenceable<T> -> bool
//
// * arithmetic sequence is defined by this formula: a[N+1] = a[N] + step
// * Sequenceable<T> means Type<T, Tag, Skill::Sequenceable>
// * N means value of type T
// * cmp is one of <, <=, ==, !=, >=, > operators
//
template <typename Derived>
struct Sequenceable : Comparable<Derived>
{
    Derived& operator++() noexcept;
    Derived operator++(int) noexcept;
    Derived& operator--() noexcept;
    Derived operator--(int) noexcept;

    template <typename T>
    std::enable_if_t<!std::is_same<std::decay_t<T>, Derived>::value, Derived&> operator+=(T&& rhs) noexcept;
    template <typename T>
    std::enable_if_t<!std::is_same<std::decay_t<T>, Derived>::value, Derived&> operator-=(T&& rhs) noexcept;

    template <typename T>
    friend constexpr std::enable_if_t<!std::is_same<std::decay_t<T>, Derived>::value, Derived> operator+(const Derived& lhs, T&& rhs) noexcept
    {
        return Derived{*lhs + std::forward<T>(rhs)};
    }
    template <typename T>
    friend constexpr std::enable_if_t<!std::is_same<std::decay_t<T>, Derived>::value, Derived> operator-(const Derived& lhs, T&& rhs) noexcept
    {
        return Derived{*lhs - std::forward<T>(rhs)};
    }

    friend constexpr auto operator-(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs - *rhs;
    }
};

template <typename Derived>
Derived& Sequenceable<Derived>::operator++() noexcept
{
    ++(*static_cast<Derived&>(*this));
    return static_cast<Derived&>(*this);
}

template <typename Derived>
Derived Sequenceable<Derived>::operator++(int) noexcept
{
    return Derived{(*static_cast<Derived&>(*this))++};
}

template <typename Derived>
Derived& Sequenceable<Derived>::operator--() noexcept
{
    --(*static_cast<Derived&>(*this));
    return static_cast<Derived&>(*this);
}

template <typename Derived>
Derived Sequenceable<Derived>::operator--(int) noexcept
{
    return Derived{(*static_cast<Derived&>(*this))--};
}

template <typename Derived>
template <typename T>
std::enable_if_t<!std::is_same<std::decay_t<T>, Derived>::value, Derived&> Sequenceable<Derived>::operator+=(T&& rhs) noexcept
{
    (*static_cast<Derived&>(*this)) += std::forward<T>(rhs);
    return static_cast<Derived&>(*this);
}

template <typename Derived>
template <typename T>
std::enable_if_t<!std::is_same<std::decay_t<T>, Derived>::value, Derived&> Sequenceable<Derived>::operator-=(T&& rhs) noexcept
{
    (*static_cast<Derived&>(*this)) -= std::forward<T>(rhs);
    return static_cast<Derived&>(*this);
}

}//namespace LibStrong::Skill
}//namespace LibStrong

#endif//SEQUENCEABLE_HH_947C330D84CD8E902561DCB5BB5CBD74
