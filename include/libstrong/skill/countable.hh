/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COUNTABLE_HH_C0DF2BB87A944DFD32EAC56B91EA77B1//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COUNTABLE_HH_C0DF2BB87A944DFD32EAC56B91EA77B1

#include "libstrong/skill/comparable.hh"

#include <type_traits>
#include <utility>

namespace LibStrong {
namespace Skill {

// Skill Countable offers operations adapted to countable entities, it means:
//
//   1) addition with the same type: Countable<T> + Countable<T> -> Countable<T>
//   2) subtraction with the same type: Countable<T> - Countable<T> -> Countable<T>
//   3) (pre/post)increment:
//      * increment count by a defined step
//   4) (pre/post)decrement:
//      * decrement count by a defined step
//   5) comparison operations: Countable<T> cmp Countable<T> -> bool
//      * compares both counts
//
// enabled operations:
//     Countable<T>  +  Countable<T> -> Countable<T>
//     Countable<T>  -  Countable<T> -> Countable<T>
//     Countable<T>  -  Countable<T> -> T
//   ++Countable<T>                  -> Countable<T>
//     Countable<T>++                -> Countable<T>
//   --Countable<T>                  -> Countable<T>
//     Countable<T>--                -> Countable<T>
//     Countable<T> cmp Countable<T> -> bool
//
// * Countable<T> means Type<T, Tag, Skill::Countable>
// * cmp is one of <, <=, ==, !=, >=, > operators
//
template <typename Derived>
struct Countable : Comparable<Derived>
{
    Derived& operator++() noexcept;
    Derived operator++(int) noexcept;
    Derived& operator--() noexcept;
    Derived operator--(int) noexcept;

    Derived& operator+=(Derived rhs) noexcept;
    Derived& operator-=(Derived rhs) noexcept;

    friend constexpr Derived operator+(Derived lhs, Derived rhs) noexcept
    {
        return Derived{*lhs + *rhs};
    }
    friend constexpr Derived operator-(Derived lhs, Derived rhs) noexcept
    {
        return Derived{*lhs - *rhs};
    }
};

template <typename Derived>
Derived& Countable<Derived>::operator++() noexcept
{
    ++(*static_cast<Derived&>(*this));
    return static_cast<Derived&>(*this);
}

template <typename Derived>
Derived Countable<Derived>::operator++(int) noexcept
{
    return Derived{(*static_cast<Derived&>(*this))++};
}

template <typename Derived>
Derived& Countable<Derived>::operator--() noexcept
{
    --(*static_cast<Derived&>(*this));
    return static_cast<Derived&>(*this);
}

template <typename Derived>
Derived Countable<Derived>::operator--(int) noexcept
{
    return Derived{(*static_cast<Derived&>(*this))--};
}

template <typename Derived>
Derived& Countable<Derived>::operator+=(Derived rhs) noexcept
{
    (*static_cast<Derived&>(*this)) += *rhs;
    return static_cast<Derived&>(*this);
}

template <typename Derived>
Derived& Countable<Derived>::operator-=(Derived rhs) noexcept
{
    (*static_cast<Derived&>(*this)) -= *rhs;
    return static_cast<Derived&>(*this);
}

}//namespace LibStrong::Skill
}//namespace LibStrong

#endif//COUNTABLE_HH_C0DF2BB87A944DFD32EAC56B91EA77B1
