/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMPARABLE_HH_A2F59A0082D0BD1D9CCF17FC636AA259//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define COMPARABLE_HH_A2F59A0082D0BD1D9CCF17FC636AA259

namespace LibStrong {
namespace Skill {

template <typename Derived>
struct Comparable
{
    friend constexpr bool operator<(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs < *rhs;
    }
    friend constexpr bool operator<=(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs <= *rhs;
    }
    friend constexpr bool operator==(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs == *rhs;
    }
    friend constexpr bool operator!=(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs != *rhs;
    }
    friend constexpr bool operator>=(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs >= *rhs;
    }
    friend constexpr bool operator>(const Derived& lhs, const Derived& rhs) noexcept
    {
        return *lhs > *rhs;
    }
};

}//namespace LibStrong::Skill
}//namespace LibStrong

#endif//COMPARABLE_HH_A2F59A0082D0BD1D9CCF17FC636AA259
