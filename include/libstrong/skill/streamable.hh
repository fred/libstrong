/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STREAMABLE_HH_359ABBCD4023E6F47A86ADA8C33B376B//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define STREAMABLE_HH_359ABBCD4023E6F47A86ADA8C33B376B

#include <ostream>

namespace LibStrong {
namespace Skill {

template <typename Derived>
struct Streamable
{
    template <class CharT, class Traits>
    friend std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& out, const Derived& value)
    {
        if (out.good())
        {
            out << *value;
        }
        return out;
    }
};

}//namespace LibStrong::Skill
}//namespace LibStrong

#endif//STREAMABLE_HH_359ABBCD4023E6F47A86ADA8C33B376B
