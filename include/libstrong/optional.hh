/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OPTIONAL_HH_3F70C1817FC8780B5D064C5D8F4894C1//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define OPTIONAL_HH_3F70C1817FC8780B5D064C5D8F4894C1

#include "libstrong/type.hh"

#include <boost/optional.hpp>

#include <exception>
#include <functional>
#include <initializer_list>
#include <type_traits>
#include <utility>

namespace LibStrong {

struct BadOptionalAccess : std::exception
{ };

#if BOOST_VERSION >= 106300
#define LIBSTRONG_HAS_IN_PLACE
struct InPlaceT
{
    constexpr explicit InPlaceT(int) { }
};
static constexpr auto in_place = InPlaceT{0};
#endif

// The main goal of LibStrong::Optional is to replace the implicit constructor from
// boost::none by the implicit constructor from LibStrong::Type<T>::nullopt. See:
//
//   void foo(boost::optional<Width>, boost::optional<Height>);
//   foo(boost::none, Height{5});
//
//   void foo(LibStrong::Optional<Width>, LibStrong::Optional<Height>);
//   foo(Width::nullopt, Height{5});
//
template <typename T, typename Tag, template <typename> class Skills>
class Optional<Type<T, Tag, Skills>>
{
public:
    using Type = LibStrong::Type<T, Tag, Skills>;
    using NullOptT = typename Type::NullOptT;
    static constexpr auto nullopt = Type::nullopt;
    constexpr Optional() noexcept
        : raw_optional_{}
    { }
    constexpr Optional(NullOptT) noexcept
        : raw_optional_{boost::none}
    { }

    template <typename _T = Type, std::enable_if_t<std::is_copy_constructible<_T>::value>* = nullptr>
    constexpr Optional(const Optional& src) noexcept(std::is_nothrow_copy_constructible<Type>::value)
        : raw_optional_{src.raw_optional_}
    { }
    template <typename _T = Type, std::enable_if_t<!std::is_copy_constructible<_T>::value>* = nullptr>
    Optional(const Optional&) = delete;

    template <typename _T = Type, std::enable_if_t<std::is_move_constructible<_T>::value>* = nullptr>
    constexpr Optional(Optional&& src) noexcept(std::is_nothrow_move_constructible<Type>::value)
        : raw_optional_{std::move(src.raw_optional_)}
    { }

    template <typename _T = Type, std::enable_if_t<std::is_copy_constructible<_T>::value>* = nullptr>
    constexpr Optional(const Type& value) noexcept(std::is_nothrow_copy_constructible<Type>::value)
        : raw_optional_{value}
    { }

    template <typename _T = Type, std::enable_if_t<std::is_move_constructible<_T>::value>* = nullptr>
    constexpr Optional(Type&& value) noexcept(std::is_nothrow_move_constructible<Type>::value)
        : raw_optional_{std::move(value)}
    { }
#ifdef LIBSTRONG_HAS_IN_PLACE
    template <typename ...Args, std::enable_if_t<std::is_constructible<Type, Args...>::value>* = nullptr>
    constexpr explicit Optional(InPlaceT, Args&& ...args) noexcept(std::is_nothrow_constructible<Type, Args...>::value)
        : raw_optional_{boost::in_place_init, std::forward<Args>(args)...}
    { }
    template <typename U, typename ...Args,
              std::enable_if_t<std::is_constructible<Type, std::initializer_list<U>&, Args...>::value>* = nullptr>
    constexpr explicit Optional(InPlaceT, std::initializer_list<U> ilist, Args&& ...args) noexcept(std::is_nothrow_constructible<
                                                                                                           Type,
                                                                                                           std::initializer_list<U>&,
                                                                                                           Args...>::value)
        : raw_optional_{boost::in_place_init, ilist, std::forward<Args>(args)...}
    { }
#endif
    constexpr Optional(bool cond, std::function<Type()> on_true)
        : Optional{cond ? Optional{on_true()}
                        : nullopt}
    { }

    Optional& operator=(NullOptT) noexcept
    {
        raw_optional_ = boost::none;
        return *this;
    }
    template <typename _T = Type>
    std::enable_if_t<std::is_copy_assignable<_T>::value,
                    Optional&> operator=(const Type& value) noexcept(std::is_nothrow_copy_assignable<Type>::value)
    {
        raw_optional_ = value;
        return *this;
    }
    template <typename _T = Type>
    std::enable_if_t<std::is_move_assignable<_T>::value,
                     Optional&> operator=(Type&& value) noexcept(std::is_nothrow_move_assignable<Type>::value)
    {
        raw_optional_ = std::move(value);
        return *this;
    }

    template <typename ...Args>
    Optional& emplace(Args&& ...args)
    {
        return *this = Type{std::forward<Args>(args)...};
    }
    template <typename U, typename ...Args>
    std::enable_if_t<std::is_constructible<Type, std::initializer_list<U>&, Args...>::value,
                     Optional&>
    emplace(std::initializer_list<U> ilist,
            Args&& ...args)
    {
        return *this = Type{ilist, std::forward<Args>(args)...};
    }

    void reset() noexcept
    {
        raw_optional_ = boost::none;
    }

    std::enable_if_t<std::is_move_constructible<Type>::value && std::is_move_assignable<Type>::value>
    swap(Optional& other) noexcept(std::is_nothrow_move_constructible<Type>::value && std::is_nothrow_move_assignable<Type>::value)
    {
        raw_optional_.swap(other.raw_optional_);
    }

    const Type& operator*() const&
    {
        if (*this != nullopt)
        {
            return *raw_optional_;
        }
        throw_bad_optional_access();
    }
    Type& operator*() &
    {
        if (*this != nullopt)
        {
            return *raw_optional_;
        }
        throw_bad_optional_access();
    }
    Type&& operator*() &&
    {
        if (*this != nullopt)
        {
            return std::move(*std::move(raw_optional_));
        }
        throw_bad_optional_access();
    }
    const Type* operator->() const
    {
        if (*this != nullopt)
        {
            return raw_optional_.operator->();
        }
        throw_bad_optional_access();
    }
    Type* operator->()
    {
        if (*this != nullopt)
        {
            return raw_optional_.operator->();
        }
        throw_bad_optional_access();
    }

    const Type& value() const
    {
        return **this;
    }
private:
    using RawOptional = boost::optional<Type>;
    RawOptional raw_optional_;

    [[noreturn]] static void throw_bad_optional_access()
    {
        struct BadOptionalAccessImpl : BadOptionalAccess
        {
            const char* what() const noexcept override
            {
                return "optional has no value";
            }
        };
        throw BadOptionalAccessImpl{};
    }

    friend const Type& get(const Optional& src)
    {
        return *src;
    }
    friend Type& get(Optional& src)
    {
        return *src;
    }
    friend Type&& get(Optional&& src)
    {
        return std::move(*std::move(src));
    }

    friend bool operator==(const Optional& lhs, NullOptT) noexcept
    {
        return lhs.raw_optional_ == boost::none;
    }
    friend bool operator==(NullOptT, const Optional& rhs) noexcept
    {
        return boost::none == rhs.raw_optional_;
    }
    friend bool operator!=(const Optional& lhs, NullOptT) noexcept
    {
        return lhs.raw_optional_ != boost::none;
    }
    friend bool operator!=(NullOptT, const Optional& rhs) noexcept
    {
        return boost::none != rhs.raw_optional_;
    }
    friend constexpr bool operator<(const Optional&, NullOptT) noexcept
    {
        return false;
    }
    friend bool operator<(NullOptT, const Optional& rhs) noexcept
    {
        return rhs != nullopt;
    }
    friend bool operator<=(const Optional& lhs, NullOptT) noexcept
    {
        return lhs == nullopt;
    }
    friend constexpr bool operator<=(NullOptT, const Optional&) noexcept
    {
        return true;
    }
    friend constexpr bool operator>=(const Optional&, NullOptT) noexcept
    {
        return true;
    }
    friend bool operator>=(NullOptT, const Optional& rhs) noexcept
    {
        return rhs == nullopt;
    }
    friend bool operator>(const Optional& lhs, NullOptT) noexcept
    {
        return lhs != nullopt;
    }
    friend constexpr bool operator>(NullOptT, const Optional&) noexcept
    {
        return false;
    }

    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() < std::declval<_T>()), bool>::value, bool>
    operator<(const Optional& lhs, const Optional& rhs)
    {
        return lhs != nullopt ? (rhs != nullopt ? *lhs < *rhs
                                                : false)
                              : rhs != nullopt;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() <= std::declval<_T>()), bool>::value, bool>
    operator<=(const Optional& lhs, const Optional& rhs)
    {
        return lhs != nullopt ? (rhs != nullopt ? *lhs <= *rhs
                                                : false)
                              : true;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() == std::declval<_T>()), bool>::value, bool>
    operator==(const Optional& lhs, const Optional& rhs)
    {
        return lhs != nullopt ? (rhs != nullopt ? *lhs == *rhs
                                                : false)
                              : rhs == nullopt;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() != std::declval<_T>()), bool>::value, bool>
    operator!=(const Optional& lhs, const Optional& rhs)
    {
        return lhs != nullopt ? (rhs != nullopt ? *lhs != *rhs
                                                : true)
                              : rhs != nullopt;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() >= std::declval<_T>()), bool>::value, bool>
    operator>=(const Optional& lhs, const Optional& rhs)
    {
        return lhs != nullopt ? (rhs != nullopt ? *lhs >= *rhs
                                                : true)
                              : rhs == nullopt;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() > std::declval<_T>()), bool>::value, bool>
    operator>(const Optional& lhs, const Optional& rhs)
    {
        return lhs != nullopt ? (rhs != nullopt ? *lhs > *rhs
                                                : true)
                              : false;
    }

    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() < std::declval<_T>()), bool>::value, bool>
    operator<(const Optional& lhs, const Type& rhs)
    {
        return lhs != nullopt ? (*lhs < rhs)
                              : true;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() < std::declval<_T>()), bool>::value, bool>
    operator<(const Type& lhs, const Optional& rhs)
    {
        return rhs != nullopt ? (lhs < *rhs)
                              : false;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() <= std::declval<_T>()), bool>::value, bool>
    operator<=(const Optional& lhs, const Type& rhs)
    {
        return lhs != nullopt ? (*lhs <= rhs)
                               : true;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() <= std::declval<_T>()), bool>::value, bool>
    operator<=(const Type& lhs, const Optional& rhs)
    {
        return rhs != nullopt ? (lhs <= *rhs)
                              : false;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() == std::declval<_T>()), bool>::value, bool>
    operator==(const Optional& lhs, const Type& rhs)
    {
        return lhs != nullopt ? (*lhs == rhs)
                              : false;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() == std::declval<_T>()), bool>::value, bool>
    operator==(const Type& lhs, const Optional& rhs)
    {
        return rhs != nullopt ? (lhs == *rhs)
                              : false;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() != std::declval<_T>()), bool>::value, bool>
    operator!=(const Optional& lhs, const Type& rhs)
    {
        return lhs != nullopt ? (*lhs != rhs)
                              : true;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() != std::declval<_T>()), bool>::value, bool>
    operator!=(const Type& lhs, const Optional& rhs)
    {
        return rhs != nullopt ? (lhs != *rhs)
                              : true;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() >= std::declval<_T>()), bool>::value, bool>
    operator>=(const Optional& lhs, const Type& rhs)
    {
        return lhs != nullopt ? (*lhs >= rhs)
                              : false;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() >= std::declval<_T>()), bool>::value, bool>
    operator>=(const Type& lhs, const Optional& rhs)
    {
        return rhs != nullopt ? (lhs >= *rhs)
                              : true;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() > std::declval<_T>()), bool>::value, bool>
    operator>(const Optional& lhs, const Type& rhs)
    {
        return lhs != nullopt ? (*lhs > rhs)
                               : false;
    }
    template <typename _T = const Type&>
    friend std::enable_if_t<std::is_same<decltype(std::declval<_T>() > std::declval<_T>()), bool>::value, bool>
    operator>(const Type& lhs, const Optional& rhs)
    {
        return rhs != nullopt ? (lhs > *rhs)
                              : true;
    }

    template <typename _T = Type>
    friend std::enable_if_t<std::is_move_constructible<_T>::value && std::is_move_assignable<_T>::value>
    swap(Optional& lhs, Optional& rhs) noexcept(std::is_nothrow_move_constructible<Type>::value && std::is_nothrow_move_assignable<Type>::value)
    {
        lhs.swap(rhs);
    }

    template <class CharT, class Traits>
    friend std::enable_if_t<std::is_same<decltype(std::declval<std::basic_ostream<CharT, Traits>&>() << std::declval<const Type&>()),
                                         std::basic_ostream<CharT, Traits>&>::value,
                            std::basic_ostream<CharT, Traits>&>
    operator<<(std::basic_ostream<CharT, Traits>& out, const Optional& value)
    {
        if (out.good())
        {
            if (value == Type::nullopt)
            {
                out << "--";
            }
            else
            {
                out << ' ' << *value;
            }
        }
        return out;
    }
};

template <typename T, typename Tag, template <typename> class Skills>
Optional<Type<T, Tag, Skills>> make_optional(Type<T, Tag, Skills>&& value)
{
    return Optional<Type<T, Tag, Skills>>{std::move(value)};
}

template <typename T, typename Tag, template <typename> class Skills>
Optional<Type<T, Tag, Skills>> make_optional(const Type<T, Tag, Skills>& value)
{
    return Optional<Type<T, Tag, Skills>>{value};
}

template <typename T, typename Tag, template <typename> class Skills>
Optional<Type<T, Tag, Skills>> make_optional(bool cond, std::function<Type<T, Tag, Skills>()> on_true)
{
    return Optional<Type<T, Tag, Skills>>{cond, on_true};
}

template <typename T, typename Tag, template <typename> class Skills>
bool is_nullopt(const Optional<Type<T, Tag, Skills>>& opt) noexcept
{
    return opt == Type<T, Tag, Skills>::nullopt;
}

template <typename T, typename Tag, template <typename> class Skills>
bool has_value(const Optional<Type<T, Tag, Skills>>& opt) noexcept
{
    return opt != Type<T, Tag, Skills>::nullopt;
}

}//namespace LibStrong

#endif//OPTIONAL_HH_3F70C1817FC8780B5D064C5D8F4894C1
