/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TYPE_HH_9460A490234B8DE81D50B83C77C69920//date "+%s.%N"|md5sum|tr "[a-f]" "[A-F]"
#define TYPE_HH_9460A490234B8DE81D50B83C77C69920

#include "libstrong/collect.hh"

#include "libstrong/skill/comparable.hh"
#include "libstrong/skill/countable.hh"
#include "libstrong/skill/no_skill.hh"
#include "libstrong/skill/sequenceable.hh"
#include "libstrong/skill/streamable.hh"

#include <initializer_list>
#include <string>
#include <type_traits>
#include <utility>

namespace LibStrong {

template <typename> class Optional;

template <typename T, typename Tag, template <typename> class Skills = Skill::NoSkill>
class Type : public Skills<Type<T, Tag, Skills>>
{
public:
    static_assert(!std::is_reference<T>::value, "T can not be a reference");
    using ValueType = T;
    using ValueTypeRef = std::add_lvalue_reference_t<ValueType>;
    using ValueTypeCRef = std::add_lvalue_reference_t<std::add_const_t<ValueType>>;
    using ValueTypeRRef = std::add_rvalue_reference_t<ValueType>;
    using ValueTypePtr = std::add_pointer_t<ValueType>;
    using ValueTypeCPtr = std::add_pointer_t<std::add_const_t<ValueType>>;

    template <typename _T = ValueType, std::enable_if_t<std::is_copy_constructible<_T>::value>* = nullptr>
    constexpr Type(const Type& src) noexcept(std::is_nothrow_copy_constructible<_T>::value)
        : value_{*src}
    { }
    template <typename _T = ValueType, std::enable_if_t<!std::is_copy_constructible<_T>::value>* = nullptr>
    Type(const Type&) = delete;

    template <typename _T = ValueType, std::enable_if_t<std::is_move_constructible<_T>::value>* = nullptr>
    constexpr Type(Type&& src) noexcept(std::is_nothrow_move_constructible<ValueType>::value)
        : value_{*std::move(src)}
    { }
    template <typename _T = ValueType, std::enable_if_t<!std::is_move_constructible<_T>::value>* = nullptr>
    Type(Type&&) = delete;

    template <typename ...Args, std::enable_if_t<std::is_constructible<ValueType, Args...>::value>* = nullptr>
    explicit constexpr Type(Args&& ...args) noexcept(std::is_nothrow_constructible<ValueType, Args...>::value)
        : value_{std::forward<Args>(args)...}
    { }
    template <typename U, typename ...Args,
              std::enable_if_t<std::is_constructible<ValueType, std::initializer_list<U>&, Args...>::value>* = nullptr>
    explicit constexpr Type(std::initializer_list<U> ilist, Args&& ...args) noexcept(std::is_nothrow_constructible<
                                                                                            ValueType,
                                                                                            std::initializer_list<U>&,
                                                                                            Args...
                                                                                     >::value)
        : value_{ilist, std::forward<Args>(args)...}
    { }

    template <typename ...Args>
    Type& emplace(Args&& ...args) noexcept(std::is_nothrow_constructible<ValueType, Args...>::value &&
                                           std::is_nothrow_move_assignable<ValueType>::value)
    {
        value_ = ValueType{std::forward<Args>(args)...};
        return *this;
    }
    template <typename U, typename ...Args>
    std::enable_if_t<std::is_constructible<ValueType, std::initializer_list<U>&, Args...>::value, Type&>
    emplace(std::initializer_list<U> ilist, Args&& ...args) noexcept(std::is_nothrow_constructible<
                                                                             ValueType,
                                                                             std::initializer_list<U>&,
                                                                             Args...
                                                                     >::value &&
                                                                     std::is_nothrow_move_assignable<ValueType>::value)
    {
        value_ = ValueType{ilist, std::forward<Args>(args)...};
        return *this;
    }
    void swap(Type& arg) noexcept(std::is_nothrow_move_constructible<ValueType>::value &&
                                  std::is_nothrow_move_assignable<ValueType>::value)
    {
        std::swap(value_, arg.value_);
    }
    constexpr ValueTypeCRef operator*() const& noexcept
    {
        return value_;
    }
    constexpr ValueTypeRef operator*() & noexcept
    {
        return value_;
    }
    constexpr std::enable_if_t<std::is_move_constructible<T>::value, ValueTypeRRef> operator*() && noexcept(std::is_nothrow_move_constructible<T>::value)
    {
        return std::move(value_);
    }
    constexpr ValueTypeCPtr operator->() const noexcept
    {
        return &value_;
    }
    constexpr ValueTypePtr operator->() noexcept
    {
        return &value_;
    }
    struct NullOptT
    {
        constexpr explicit NullOptT(int) { }
    };
    static constexpr auto nullopt = NullOptT{0};
    using Optional = LibStrong::Optional<Type>;
private:
    ValueType value_;
};

template <typename T, typename Tag, template <typename> class Skills>
typename Type<T, Tag, Skills>::ValueTypeCRef get(const Type<T, Tag, Skills>& strong_value)
{
    return *strong_value;
}

template <typename T, typename Tag, template <typename> class Skills>
typename Type<T, Tag, Skills>::ValueTypeRef get(Type<T, Tag, Skills>& strong_value)
{
    return *strong_value;
}

template <typename T, typename Tag, template <typename> class Skills>
typename Type<T, Tag, Skills>::ValueTypeRRef get(Type<T, Tag, Skills>&& strong_value)
{
    return std::move(*std::move(strong_value));
}

template <typename T, typename Tag, template <typename> class Skills>
typename Type<T, Tag, Skills>::ValueTypeCPtr get(const Type<T, Tag, Skills>* strong_value_ptr)
{
    return strong_value_ptr == nullptr ? nullptr
                                       : strong_value_ptr->operator->();
}

template <typename T, typename Tag, template <typename> class Skills>
typename Type<T, Tag, Skills>::ValueTypePtr get(Type<T, Tag, Skills>* strong_value_ptr)
{
    return strong_value_ptr == nullptr ? nullptr
                                       : strong_value_ptr->operator->();
}

template <typename T, typename Tag, template <typename> class ...Skills>
using TypeWithSkills = Type<T, Tag, Collect<Skills...>::template SuperSkill>;

template <typename T, typename Tag>
using ArithmeticSequence = TypeWithSkills<T, Tag, Skill::Sequenceable, Skill::Streamable>;

}//namespace LibStrong

#include "libstrong/optional.hh"

#endif//TYPE_HH_9460A490234B8DE81D50B83C77C69920
