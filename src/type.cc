/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "libstrong/type.hh"

#include <type_traits>

namespace LibStrong {
namespace {

template <typename, typename = void> struct CheckComparisonOperators;

template <typename T, typename Tag, template <typename> class Skills>
struct CheckComparisonOperators<Type<T, Tag, Skills>, std::enable_if_t<std::is_integral<T>::value>>
{
    using StrongType = Type<T, Tag, Skills>;
    static_assert(*StrongType{1} == 1, "must be '1'");

    static_assert(StrongType{1} < StrongType{2}, "'1' must be lesser than '2'");
    static_assert(!(StrongType{1} < StrongType{1}), "'1' can not be lesser than '1'");
    static_assert(!(StrongType{2} < StrongType{1}), "'2' can not be lesser than '1'");
    static_assert(StrongType{1} <= StrongType{2}, "'1' must be lesser than or equal to '2'");
    static_assert(StrongType{1} <= StrongType{1}, "'1' must be lesser than or equal to '1'");
    static_assert(!(StrongType{2} <= StrongType{1}), "'2' can not be lesser than nor equal to '1'");
    static_assert(!(StrongType{1} == StrongType{2}), "'1' can not be equal to '2'");
    static_assert(StrongType{1} == StrongType{1}, "'1' must be equal to '1'");
    static_assert(!(StrongType{2} == StrongType{1}), "'2' can not be equal to '1'");
    static_assert(StrongType{1} != StrongType{2}, "'1' must differ from '2'");
    static_assert(!(StrongType{1} != StrongType{1}), "'1' can not differ from '1'");
    static_assert(StrongType{2} != StrongType{1}, "'2' must differ from '1'");
    static_assert(!(StrongType{1} >= StrongType{2}), "'1' can not be greater than nor equal to '2'");
    static_assert(StrongType{1} >= StrongType{1}, "'1' must be greater than or equal to '1'");
    static_assert(StrongType{2} >= StrongType{1}, "'2' must be greater than or equal to '1'");
    static_assert(!(StrongType{1} > StrongType{2}), "'1' can not be greater than '2'");
    static_assert(!(StrongType{1} > StrongType{1}), "'1' can not be greater than '1'");
    static_assert(StrongType{2} > StrongType{1}, "'2' must be greater than '1'");
};

using Id = ArithmeticSequence<int, struct IdTag_>;

template struct CheckComparisonOperators<Id>;

static_assert(Id{1} + 2 == Id{3}, "sum must be '3'");
static_assert(Id{3} - 2 == Id{1}, "diff must be '1'");
static_assert(Id{3} - Id{2} == 1, "diff must be '1'");

static_assert(std::is_default_constructible<Id>::value, "must be default constructible");
static_assert(std::is_constructible<Id, char>::value, "must be constructible from (char)");
static_assert(std::is_constructible<Id, unsigned char>::value, "must be constructible from (unsigned char)");
static_assert(std::is_constructible<Id, long long>::value, "must be constructible from (long long)");
static_assert(std::is_constructible<Id, unsigned long long>::value, "must be constructible from (unsigned long long)");
static_assert(!std::is_constructible<Id, void*>::value, "can not be constructible from (void*)");
static_assert(!std::is_constructible<Id, int, int>::value, "can not be constructible from (int, int)");
static_assert(!std::is_constructible<Id, std::initializer_list<int>>::value, "can not be constructible from (std::initializer_list<int>)");

using OptionalId = typename Id::Optional;

static_assert(std::is_default_constructible<OptionalId>::value, "must be default constructible");
static_assert(std::is_constructible<OptionalId, Id>::value, "must be constructible from (Id)");
static_assert(std::is_constructible<OptionalId, bool, std::function<Id()>>::value, "must be constructible from (bool, std::function<Id()>)");
static_assert(std::is_constructible<OptionalId, InPlaceT>::value, "must be constructible from (InPlaceT)");
static_assert(std::is_constructible<OptionalId, InPlaceT, int>::value, "must be constructible from (InPlaceT, int)");
static_assert(!std::is_constructible<OptionalId, int>::value, "can not be constructible from (int)");
static_assert(!std::is_constructible<OptionalId, bool, int>::value, "can not be constructible from (bool, int)");

template struct CheckComparisonOperators<Type<int, struct RandomIdTag_, Skill::Comparable>>;

using Handle = Type<std::string, struct HandleTag_>;
using OptionalHandle = typename Handle::Optional;

static_assert(std::is_default_constructible<OptionalHandle>::value, "must be default constructible");
static_assert(std::is_constructible<OptionalHandle, Handle>::value, "must be constructible from (Handle)");
static_assert(std::is_constructible<OptionalHandle, bool, std::function<Handle()>>::value, "must be constructible from (bool, std::function<Handle()>)");
static_assert(std::is_constructible<OptionalHandle, InPlaceT>::value, "must be constructible from (InPlaceT)");
static_assert(std::is_constructible<OptionalHandle, InPlaceT, const char*>::value, "must be constructible from (InPlaceT, const char*)");
static_assert(!std::is_constructible<OptionalHandle, const char*>::value, "can not be constructible from (const char*)");
static_assert(!std::is_constructible<OptionalHandle, bool, const char*>::value, "can not be constructible from (bool, const char*)");

using Size = Type<int, struct SizeTag_, Skill::Countable>;

template struct CheckComparisonOperators<Size>;

static_assert(Size{1} + Size{2} == Size{3}, "sum must be '3'");
static_assert(Size{3} - Size{2} == Size{1}, "diff must be '1'");

}//namespace LibStrong::{anonymous}
}//namespace LibStrong
